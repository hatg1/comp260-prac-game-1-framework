﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}



	public float maxSpeed = 5.0f;
	public float acceleration = 10.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float brake = 100.0f;
	public float turnSpeed = 30.0f;
	public float proportionateTurn;
	public string Xaxis;
	public string Yaxis;

	// Update is called once per frame
	void Update () {
		proportionateTurn = turnSpeed - (speed * 0.2f);
		float turn = Input.GetAxis (Xaxis);
		transform.Rotate (0, 0, -turn * proportionateTurn * Time.deltaTime);
		float forwards = Input.GetAxis (Yaxis);
		if (forwards > 0) {
			speed = speed + acceleration * Time.deltaTime;
		} else if (forwards < 0) {
			speed = speed - acceleration * Time.deltaTime;
		} else {
			if (speed > 0) {
				speed = Mathf.Max((speed - brake * Time.deltaTime),0);
			} else {
				speed = Mathf.Min((speed + brake * Time.deltaTime),0);
			}
		}
		speed = Mathf.Clamp (speed, -maxSpeed, maxSpeed);
		Vector2 velocity = Vector2.up * speed;



		transform.Translate (velocity * Time.deltaTime, Space.Self);
	}
}
